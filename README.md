# Pythia: consult the oracle!

This is self-education exercise: can a Ruby library that relies on
extensive metaprogramming and some fancy magic be expressed as a DSL
in Haskell? What type can we give the operators?

A secondary question is, since the library is a build-system for
propositions, can it be expressed in the taxonomy of the `Build` system
paper.

The library in question is
[`declarative-policy`](https://gitlab.com/gitlab-org/declarative-policy), a
permissions and authorization library in use throughout gitlab-org/gitlab>.

## Results:

So far things look promising!

