{-# LANGUAGE DeriveFunctor, DeriveAnyClass, ConstraintKinds, RecordWildCards #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DerivingStrategies, GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ExistentialQuantification, DefaultSignatures #-}
{-# LANGUAGE RankNTypes, LambdaCase, ScopedTypeVariables, FunctionalDependencies #-}

module Pythia.Oracle where

import qualified Data.ByteString.Base64 as Base64
import Data.Text (Text)
import qualified Data.Text as T
import Type.Reflection (typeRep, Typeable, TypeRep)
import Data.ByteString.Builder as B (byteString, stringUtf8, int64BE, toLazyByteString)
import Data.ByteString (ByteString)
import Data.ByteString.Lazy as B (toStrict)
import qualified Crypto.Hash.SHA1 as SHA1
import GHC.Stack
import Data.Maybe (listToMaybe, fromMaybe, isJust)
import Control.Monad.Extra (ifM)
import Data.Hashable (Hashable)
import Data.Foldable (foldl')
import Data.List.Extra (lower, intercalate)
import Control.Monad.Reader
import Control.Monad.State.Strict
import qualified Data.HashPSQ as PQ
import qualified Data.HashMap.Strict as Map
import Data.HashMap.Strict (HashMap)
import Data.List.NonEmpty (NonEmpty((:|)))
import qualified Data.List.NonEmpty as NE
import qualified Control.Concurrent.Map as Cache

data Outcome = Prevented String | Enabled String | NotEnabled

type Ability a = (Eq a, Hashable a, Show a)

class (CacheKey s, CacheKey o, Ability v) => HasPolicy s v o m | s o -> v where
  policy :: Rules m s v o 

data Judgements a = Judgements { enabled :: [a], disabled :: [a] }
  deriving Functor

invert :: Judgements a -> Judgements a
invert (Judgements xs ys) = Judgements ys xs

allow :: a -> Judgements a
allow x = Judgements [x] []

deny :: a -> Judgements a
deny = invert . allow

instance Semigroup (Judgements a) where
  (<>) a b = Judgements (enabled a ++ enabled b) (disabled a ++ disabled b)

instance Monoid (Judgements a) where
  mempty = Judgements [] []

data Rules m s v o = Rules
  { getRules :: HashMap v (Judgements (Fact m s v o))
  , delegates :: [Delegate m s v o]
  , policyHash :: ByteString
  }

data Delegate m s v o = forall ability delegate. (Ability ability, CacheKey delegate) => Delegate
  { delegatedObject :: Determination m s o delegate
  , mapAbility :: v -> ability
  , delegatedRules :: Rules m s ability delegate
  }

data PropositionKind = Transitive | Ergative | Intransitive | Impersonal

type Cache = Cache.Map

-- TODO: track dirty keys written to cache - clear on each entry to `evalRule`
newtype Facts = Facts { getFacts :: Cache Text Bool }

data LogicalOp = AND | OR deriving (Eq, Show)

data Fact m s v o
  = BaseFact (Condition m s v o)
  | Composite LogicalOp (NonEmpty (Fact m s v o))
  | Negated (Fact m s v o)
  | Implication v
  | Absurd

instance Semigroup (Fact m s v o) where
  -- straigh up joins
  Composite AND as <> Composite AND bs = Composite AND (as <> bs)
  Composite OR as <> Composite OR bs = Composite OR (as <> bs)
  -- push negation down to get larger composites
  Composite AND as <> (Negated (Composite OR bs)) = Composite AND (as <> fmap Negated bs)
  Negated (Composite OR as) <> Composite AND bs = Composite AND (fmap Negated as <> bs)
  Composite OR as <> (Negated (Composite AND bs)) = Composite OR (as <> fmap Negated bs)
  Negated (Composite AND as) <> Composite OR bs = Composite OR (fmap Negated as <> bs)
  -- <> means AND
  Composite AND as <> b = Composite AND (as <> pure b)
  a <> Composite AND bs = Composite AND (pure a <> bs)
  a <> b = Composite AND (pure a <> pure b)

data Condition m s v o = Condition
  { name :: String
  , identityHash :: ByteString
  , statement :: Determination m s o Bool
  , score :: Maybe Int
  , propKind :: PropositionKind
  }

class Factish t where
  fact :: t m s v o -> Fact m s v o

instance Factish Condition where
  fact c = BaseFact c

instance Factish Fact where
  fact = id

-- Beware! The name of this condition must be unique within this policy
(?==) :: String -> Determination m s o Bool -> Condition m s v o
name ?== statement = Condition name (bytes name) statement Nothing Transitive

-- Beware! To use `condition`, the caller must mark the site as `HasCallStack`:
-- e.g:
--  always :: HasCallStack => Condition IO Char () Char
--  always = condition (pure True)
condition :: HasCallStack => Determination m s o Bool -> Condition m s v o
condition det = do
  let caller = listToMaybe $ getCallStack callStack
      -- name = maybe "unknown" fst caller
      name = intercalate "." . fmap fst $ getCallStack callStack
      loc = maybe "unknown" (show . fst) caller
   in (name ?== det) { identityHash = SHA1.hash (bytes (name <> loc)) }

bytes :: String -> ByteString
bytes = B.toStrict . B.toLazyByteString . B.stringUtf8

(<&>) :: (Factish a, Factish b) => a m s v o -> b m s v o -> Fact m s v o
a <&> b = composite AND a <> composite AND b

(<|>) :: (Factish a, Factish b) => a m s v o -> b m s v o -> Fact m s v o
a <|> b = composite OR a <> composite OR b

refute :: Factish a => a m s v o -> Fact m s v o
refute = Negated . fact

composite :: Factish f => LogicalOp -> f m s v o -> Fact m s v o
composite op x = case fact x of
  Composite op' fs | op' == op -> Composite op fs
  Composite _ (f :| []) -> composite op f
  f -> Composite op (pure f)

data Rule m s v o = Rule
  { observation :: Fact m s v o
  , outcome :: Outcome
  }

absurd :: Fact m s v o
absurd = Absurd

trivial :: Fact m s v o
trivial = Negated Absurd

newtype Determination m s o a = Determination
  { runDeterination :: ReaderT (s, o) m a }
  deriving newtype (Functor, Applicative, Monad, MonadReader (s, o))

instance MonadIO m => MonadIO (Determination m s o) where
  liftIO m = Determination (liftIO m)

newtype FactM s v o m a = FactM
  { runFactM :: ReaderT (Facts, Rules m s v o, s, o) m a }
  deriving newtype (Functor, Applicative, Monad, MonadReader (Facts, Rules m s v o, s, o))

instance MonadTrans (FactM s v o) where
  lift ma = FactM (lift ma)

instance MonadIO m => MonadIO (FactM s v o m) where
  liftIO m = FactM (liftIO m)

runFacts :: Facts -> Rules m s v o -> s -> o -> FactM s v o m a -> m a
runFacts fs rs s v = flip runReaderT (fs, rs, s, v) . runFactM 

viewedAs :: Monad m => (r' -> r) -> ReaderT r m a -> ReaderT r' m a
viewedAs f act = do
  r <- asks f
  lift (runReaderT act r)

currentRules :: Monad m => FactM s v o m (Rules m s v o)
currentRules = asks $ \(_, rs, _, _) -> rs

determine :: Monad m => Determination m s o a -> FactM s v o m a
determine = FactM . viewedAs view . runDeterination
  where
    view (_, _, s, o) = (s, o)

subject :: Monad m => Determination m s o s
subject = asks fst

object :: Monad m => Determination m s o o
object = asks snd

-- The default implementation is suitable for any Show instance
-- where `show a` contains all identifying information, and never
-- any extraneous information.
class CacheKey k where
  key :: k -> Text
  
  default key :: (Show k) => k -> Text
  key = T.pack . show

-- TODO: use policyHash here
cacheKey :: (CacheKey s, CacheKey o) => Condition m s v o -> Rules m s v o -> s -> o -> Text
cacheKey c rs s o =
  let k = Base64.encodeBase64 (policyHash rs <> identityHash c)
   in case propKind c of
        Impersonal -> k
        Intransitive -> T.intercalate "/" [k, key s]
        Transitive -> T.intercalate "/" [k, key s, key o]
        Ergative -> T.intercalate "/" [k, key o]

type RuleM m s v o a = State (Rules m s v o) a

delegate :: Monad m => HasPolicy s v d m => (o -> d) -> RuleM m s v o ()
delegate f = runDelegate $ Delegate (fmap f object) id policy

delegateM :: HasPolicy s v d m => Determination m s o d -> RuleM m s v o ()
delegateM det = runDelegate $ Delegate det id policy

delegateWith :: HasPolicy s a d m => (v -> a) -> Determination m s o d -> RuleM m s v o ()
delegateWith f det = runDelegate $ Delegate det f policy

runDelegate :: Delegate m s v o -> RuleM m s v o ()
runDelegate d = modify' $ \r -> r { delegates = d : delegates r }

(==>) :: (Factish f, Ability v) => f m s v o -> Judgements v -> RuleM m s v o ()
(==>) x Judgements{..}  = modify' (on enabled . off disabled)
  where
    f = fact x
    on = addJudgements (allow f)
    off = addJudgements (deny f)

addJudgements :: Ability v => Judgements (Fact m s v o) -> [v] -> Rules m s v o -> Rules m s v o
addJudgements js vs rs = foldl' (addJudgement js) rs vs

addJudgement :: Ability v => Judgements (Fact m s v o) -> Rules m s v o ->  v -> Rules m s v o
addJudgement facts rs v = rs { getRules = Map.insertWith (<>) v facts (getRules rs) }

unless :: (Factish f, Ability v) => Judgements v -> f m s v o -> RuleM m s v o ()
unless j c = refute c ==> j

provided :: (Factish f, Ability v) => Judgements v -> f m s v o -> RuleM m s v o ()
provided = flip (==>)

rules :: forall s v o m. (Ability v, Typeable s, Typeable o)
      => RuleM m s v o () -> Rules m s v o
rules builder =
  let identity = SHA1.hash . bytes $ show (typeRep :: TypeRep s) <> show (typeRep :: TypeRep o)
   in finalize $ execState builder (Rules Map.empty [] identity)
  where
    -- eliminate implications throughout the ruleset
    finalize r = let infer f = case f of
                       Implication v -> fromMaybe absurd $ do
                         Judgements{..} <- fmap infer <$> Map.lookup v (getRules r)
                         needed <- Composite OR <$> NE.nonEmpty enabled
                         case disabled of
                           [] -> pure needed
                           fs -> pure $ Composite AND (needed :| fmap Negated fs)
                       Composite op fs -> Composite op (fmap infer fs)
                       Negated f' -> Negated (infer f')
                       _ -> f
                       
                  -- TODO: simplification of rule sets here
                  in r { getRules = fmap infer <$> getRules r }

allowed :: (MonadIO m, HasPolicy s v o m) => Facts -> s -> v -> o -> m Outcome
allowed = allowed' policy

allowed' :: (CacheKey s, Ability v, CacheKey o, MonadIO m)
         => Rules m s v o -> Facts -> s -> v -> o -> m Outcome
allowed' ruleBook facts s v o = runFacts facts ruleBook s o $ 
  judge (enabling facts ruleBook v `Judgements` disabling facts ruleBook v)

judge :: (MonadIO m, CacheKey s, Ability v, CacheKey o)
      => Judgements (Fact m s v o) -> FactM s v o m Outcome 
judge Judgements{..} = do
  blockers <- sortFacts disabled
  required <- sortFacts enabled
  go NotEnabled blockers required
  where
    go NotEnabled  pqp pqe | not (PQ.null pqe) = runNextRule NotEnabled pqp pqe
    go (Enabled e) pq  _   | not (PQ.null pq) = runNextRule (Enabled e) pq PQ.empty
    go outcome _  _ = pure outcome

    runNextRule outcome pqp pqe = do
      let (rule, pqp', pqe') = nextRule pqp pqe
      result <- evalRule outcome rule
      go result pqp' pqe'

    nextRule qa qb = case (PQ.minView qa, PQ.minView qb) of
      (Nothing,             Nothing) -> (Rule absurd NotEnabled, qa, qb)
      (Just (k, _, c, qa'), Nothing) -> (Rule c (Prevented k), qa', qb)
      (Nothing, Just (k, _, c, qb')) -> (Rule c (Enabled k), qa, qb')
      (Just (kp, scorep, cp, pqp'), Just (ke, scoree, ce, pqe')) ->
        if scorep <= scoree
        then (Rule cp (Prevented kp), pqp', qb)
        else (Rule ce (Enabled ke), qa, pqe')

    evalRule ifFailed rule = do
      passed <- observe (observation rule)
      pure (if passed then outcome rule else ifFailed)

observe :: (CacheKey s, Ability v, CacheKey o, MonadIO m)
        => Fact m s v o -> FactM s v o m Bool
observe = \case
  Absurd -> pure False
  BaseFact c -> caching (cacheKey c) (statement c)
  Negated f -> not <$> observe f
  Implication v -> do
    -- non-emliminated implication: run as its own evaluation
    (fs, rs, s, o) <- ask
    outcome <- lift (allowed' rs fs s v o)
    case outcome of { Enabled _ -> pure True; _ -> pure False }
  -- these are the same as (allM/anyM run fs), but with extra ordering
  Composite AND fs -> do
    outcome <- judge (nonePrevented fs)
    case outcome of { Prevented _ -> pure False; _ -> pure True }
  Composite OR fs -> do
    outcome <- judge (oneOf fs)
    case outcome of { Enabled _ -> pure True; _ -> pure False }

nonePrevented :: NonEmpty (Fact m s v o) -> Judgements (Fact m s v o)
nonePrevented fs = Judgements { enabled = [trivial], disabled = fmap Negated (NE.toList fs) }

oneOf :: NonEmpty (Fact m s v o) -> Judgements (Fact m s v o)
oneOf fs = Judgements { enabled = NE.toList fs, disabled = [] }

disabling :: (MonadIO m, CacheKey s, Ability v) => Facts -> Rules m s v o -> v -> [Fact m s v o]
disabling fs rs v = maybe [] enabled (Map.lookup v $ getRules rs) ++ delegatedFacts disabling fs rs v

enabling :: (MonadIO m, CacheKey s, Ability v) => Facts -> Rules m s v o -> v -> [Fact m s v o]
enabling fs rs v = maybe [] enabled (Map.lookup v $ getRules rs) ++ delegatedFacts enabling fs rs v

delegatedFacts :: (HasCallStack, MonadIO m, CacheKey s, Ability v)
               => (forall u o'. Ability u => Facts -> Rules m s u o' -> u -> [Fact m s u o'])
               -> Facts -> Rules m s v o -> v -> [Fact m s v o]
delegatedFacts query facts rs v = zip [0..] (delegates rs) >>= rulesFor
  where
    rulesFor (i, Delegate{..}) = zipWith (asFact delegatedRules delegatedObject i)
                                         [0..]
                                         (query facts delegatedRules (mapAbility v))
    asFact rs' obj i j f = BaseFact $ Condition
                         { name = "delegation to " ++ factName f
                         , identityHash = B.toStrict . B.toLazyByteString $ mconcat
                                            [ B.byteString (policyHash rs')
                                            , B.int64BE (toEnum i)
                                            , B.int64BE (toEnum j)
                                            ]
                         , statement = do s <- subject
                                          o <- obj
                                          Determination . lift . runFacts facts rs' s o $ observe f
                         , score = Nothing -- TODO - make score a function
                         , propKind = Transitive -- TODO
                         }

type Steps m s v o = PQ.HashPSQ String Int (Fact m s v o)

sortFacts :: (MonadIO m, CacheKey s, Ability v, CacheKey o) => [Fact m s v o] -> FactM s v o m (Steps m s v o)
sortFacts fs = do
  elems <- traverse (\f -> fmap (\i -> (factName f, i, f)) $ factScore f) fs
  pure $ PQ.fromList elems

factName :: Show v => Fact m s v o -> String
factName Absurd = "impossible"
factName (BaseFact c) = name c
factName (Negated f) = "not " ++ factName f
factName (Implication v) = "can " ++ show v
factName (Composite op fs) = let conj = lower (show op)
                              in intercalate conj (NE.toList . fmap factName $ fs)

factScore :: (Monad m, MonadIO m, CacheKey s, CacheKey o, Ability v) => Fact m s v o -> FactM s v o m Int
factScore Absurd = pure 0
factScore (Negated f) = factScore f
factScore (Composite _ fs) = fmap sum (traverse factScore fs)
factScore (Implication v) = do
  rs <- currentRules
  -- we should never need to score an implication separately, but if we did:
  case Map.lookup v (getRules rs) of
                       Nothing -> pure 0
                       Just js -> sum <$> traverse factScore (enabled js <> disabled js)
factScore (BaseFact c) = ifM (cached c) (pure 0)
                             (case score c of
                               Nothing -> pure (defaultScore (propKind c))
                               Just i -> pure i)

-- all things being equial, prefer the more-readily shared conditions
defaultScore :: PropositionKind -> Int
defaultScore pk = case pk of
  Transitive -> 16
  Intransitive -> 8
  Ergative -> 8
  Impersonal -> 2

caching :: MonadIO m => (Rules m s v o -> s -> o -> Text) -> Determination m s o Bool -> FactM s v o m Bool
caching toKey det = do
  (Facts m, rs, s, o) <- ask
  let k = toKey rs s o 
  mr <- liftIO (Cache.lookup k m)
  case mr of
    Just r -> pure r
    Nothing -> do
      r <- determine det
      _ <- liftIO (Cache.insert k r m)
      pure r

cached :: (CacheKey s, CacheKey o, MonadIO m) => Condition m s v o -> FactM s v o m Bool
cached c = do
  (Facts m, rs, s, o) <- ask
  let k = cacheKey c rs s o 
  isJust <$> liftIO (Cache.lookup k m)

intransitive :: Monad m => Condition m s a () -> Condition m s b o
intransitive c = c
  { propKind = Intransitive
  , statement = adapt (\s _ -> (s, ())) (statement c)
  }

ergative :: Monad m => Condition m () a o -> Condition m s b o
ergative c = c
  { propKind = Intransitive
  , statement = adapt (\_ o -> ((), o)) (statement c)
  }

impersonal :: Monad m => Condition m () a () -> Condition m s b o
impersonal c = c
  { propKind = Impersonal
  , statement = adapt (\_ _ -> ((), ())) (statement c)
  }

adapt :: Monad m => (s' -> o' -> (s, o)) -> Determination m s o a -> Determination m s' o' a
adapt f det = Determination $ viewedAs (uncurry f) (runDeterination det)
