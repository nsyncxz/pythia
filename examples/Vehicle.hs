{-# OPTIONS_GHC -fplugin=RecordDotPreprocessor #-}

module Vehicle where

-- The domain
newtype ID a = ID { getID :: Int }

data User = User { userId :: ID User, dob :: Date, license :: License, trusts :: [ID User] }
data Vehicle = Vehicle { model :: Model, registration :: ID Registration, ownerId :: ID User }
data Laws = Laws { maxAlcohol :: Double, minDrivingAge :: Years }
data BloodWork = BloodWork { alcohol :: Double }
data Country = Country { laws :: Laws }
data Registration = Registration { plate :: String, expiry :: Date, countryId :: ID Country }
data License = License { licenseExpiry :: Date }

data Abilities = DriveVehicle | SellVehicle

class HasCountry m a where
  currentCountry :: a -> m Country

class HasDB a where
  db :: a -> DB

newtype DB a = DB { getEntity :: ID a -> IO (Maybe a) }

instance HasPolicy License a LicenseAbilities where
  policy _ license = rules $ do
    expiredLicense license ==> prevent DriveVehicle

instance HasPolicy Vehicle User CarAbilities where
  policy = rules $ do
    delegate (license <$> subject)

    owner     ==> enable DriveVehicle <> enable SellVehicle
    trusted   ==> enable DriveVehicle
    intoxicated ==> prevent DriveVehicle
    prevent DriveVehicle `unless` aboveLegalDrivingAge

intoxicated :: Condition m User ()
intoxicated = condition "intoxicated" ((>=) <$> userBloodAlcohol <*> bloodAlcoholLimit)
  where
    userBloodAlcohol = alcohol <$> (subject >>= fetch takeSample)
    bloodAlcoholLimit = maxAlcohol <$> fetch localDrivingLaws

owner ::Condition m s Bool
owner = condition "owner" ((==) <$> fmap userId user <*> fmap ownerId vehicle)

trusted :: HasDB s => User -> Vehicle -> Condition IO s Bool
trusted user vehicle = do
  mowner <- fetch getUser vehicle.ownerId  
  case trusts <$> mowner of
    Nothing -> pure False
    Just ids -> pure (user.userId `elem` ids)

aboveLegalDrivingAge :: (MonadTime m, HasCountry s) => User -> Condition m s Bool
aboveLegalDrivingAge user = do
  now <- currentDate
  laws <- fetch localDrivingLaws
  let age = years (now - user.dob)
  pure (age >= laws.minDrivingAge)

expiredLicense :: License -> Condition m s Bool
expiredLicense license = do
  now <- currentDate
  pure (license.licenseExpiry > now)

localDrivingLaws :: HasCountry s => Condition m s Laws
localDrivingLaws = laws . currentCountry <$> conditionState

takeSample :: User -> Condition m s BloodWork
takeSample _ = pure . BloodWork $ 0.01

getUser :: Query m s (ID User) (Maybe User)
getUser = query $ \userId -> do
  db' <- db <$> conditionState
  getEntity db' userId
